# README #

This is the raw data that accompanies the publication KOBRA: A Fluctuating Elastic Rod Model for Slender Biological Macromolecules, which you can read here: https://doi.org/10.1039/D0SM00491J

This contains everything except for the original AMBER trajectories, which are too big to fit in this repo. You can find them on the University of Leeds research data archive here: http://archive.researchdata.leeds.ac.uk/641/1/AMBER_trajectories.zip

### Dependencies ###

* Python 2.7 (for most scripts)
* Matplotlib
* Numpy
* FFEA >= 2.7 with the Python API installed, which you can get at http://ffea.bitbucket.io
* Python 3.x (for the scripts in build_ndc80c)
* ISAMBARD 1.x (the 2.x release won't work - also for the scripts in build_ndc80c)
* MDAnalysis (for parameter extraction)
* PyMOL (if you want to view trajectories)
* PyPcaZip (if you want to do PCA)
* VPython and POV-RAY (for if you want to generate those lovely 3D ray-traced plots)

### How to use ###

* With Python 2.7, Matplotlib, Numpy and FFEA, you should be able to do all the data analysis and generate the figures seen in the publication. Run plot_all.py to make them all at once.
* With Python 3 and ISAMBARD, run the script in the build_ndc80c folder to build the two halves of the Ndc80C model. The unstructured loop is not here, it was built separately using QUARK. If you want to take a look at that, check the AMBER trajetories.
* The trajectories themselves are in a folder called ndc80c_sims. The converted all-atom trajectory is here, as well as both iterations of the parameter recovery. You can view them by loading realistic.ffea in the FFEA viewer.

### Help ###

All of these trajectories and scripts come with no warrantly. Some of the code isn't particularly elegant, and it's not all well-documented. They're here for transparency and replicability but they might not be particularly easy to use. I may not be able to provide support for them. If you have questions, ask the FFEA team, who will be happy to help. Check the bottom of http://ffea.bitbucket.io for their contact details.

All the data and scripts were created by Rob Welch (https://robwel.ch). They are released under a GPLv3 software license, the same as FFEA.
