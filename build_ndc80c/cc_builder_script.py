#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 13:37:42 2018

@author: rob
"""

import isambard
# note: this script NEEDS isambard 1.x. and it will not work in 2.x, i also
# think that this functionality is not present in 2.x
# adding side chains will also require SCWRL4
import numpy

# part one: import bonsai ndc80c, extract parameters

path = "ndc80c_modelled.pdb"

ndc80_modelled = isambard.ampal.convert_pdb_to_ampal(path)

# The first chain has a continuous helix so we can get it like this
# NOTE: CC0 = NDC80, CC1 = NUF2
for helix in ndc80_modelled[0].helices:
    if "STANK" in helix.sequence:
        cc0 = helix
        
# The second one has a break in the middle, so we need to find the sequence
# some other way

# in this example I used chain.helices and located the helices I needed graphically
# their sequences are:
# part 1: 'TIPSAVYVAQLYHQVS'
# (then an unstructured bit)
# part 2: 'RKFISDYLWS'
chain2_sequence= ndc80_modelled[1].sequence
indices = list(range(len(chain2_sequence)) )

section_start_pos = chain2_sequence.find("DKMQQLNAAHQEALMKLERLE") # first part of the coiled-coil
section_end_pos = chain2_sequence.find("RKFISDYLWS")+len("RKFISDYLWS") # second part of the coiled-coil

cc1 = ndc80_modelled[1][section_start_pos:section_end_pos]

# chop them down, because one of the helices (nuf2) goes on for way longer than the other

max_length = min([len(cc0), len(cc1)])
cc0 = cc0[:max_length]
cc1 = cc1[:max_length]

# finally, we can begin the sacred coiled-coil building exercise

# extract parameters
reference_axis = isambard.analyse_protein.reference_axis_from_chains([cc0, cc1])

# crick angles
crangles_0 = isambard.analyse_protein.crick_angles(cc0,reference_axis)
crangles_1 = isambard.analyse_protein.crick_angles(cc1,reference_axis)

# average phi c-alpha value
phica_0_list = [crangles_0[x] for x in range(0,len(crangles_0),7) if crangles_0[x] is not None]
phica_1_list = [crangles_1[x] for x in range(0,len(crangles_1),7) if crangles_1[x] is not None]
phica_0 = numpy.mean(phica_0_list)
phica_1 = numpy.mean(phica_1_list)

#radius
radius_0_list = isambard.analyse_protein.polymer_to_reference_axis_distances(cc0, reference_axis)
radius_1_list = isambard.analyse_protein.polymer_to_reference_axis_distances(cc1, reference_axis)

radius = numpy.mean(radius_0_list+radius_1_list)

# pitch
alpha_0_list = isambard.analyse_protein.alpha_angles(cc0, reference_axis)
alpha_1_list = isambard.analyse_protein.alpha_angles(cc1, reference_axis)

pitch_0_list = [(2* numpy.pi * radius) / numpy.tan(numpy.deg2rad(x)) for x in alpha_0_list if x is not None]
pitch_1_list = [(2* numpy.pi * radius) / numpy.tan(numpy.deg2rad(x)) for x in alpha_1_list if x is not None]

pitch = numpy.mean(pitch_0_list + pitch_1_list)

# part two: build a new coiled-coil

class SimpleDimer2Phi(isambard.specifications.CoiledCoil):
    def __init__(self, aa, r, p, phica1,phica2,n=2):
        super().__init__(n, auto_build=False)
        self.aas = [aa]*n
        self.major_radii = [r]*n
        self.major_pitches = [p]*n
        self.phi_c_alphas = [phica1,phica2]
        self.orientations = [1]*n 
        self.build()

# left to right
n2_helix1 = "AKRTSRFLSGIINFIHFREACRETYMEFLWQYKSSADKMQQLNAAHQEALMKLERL"
n2_unstructured1 = "DSVPVEE"
n2_helix2 = "QEEFKQLSDGIQELQQSLNQDFHQKTIVLQEGNSQKKSNISEKTKRLNELKLSVVSLKEIQESLKTKI"
n2_unstructured2 = "VDS"
n2_helix3 = "PEKLKNYKEKMKDTVQKLKNARQEVVEKYEIYGDSVD"
n2_unstructured3 = "CLPS"
n2_helix4 = "CQLEVQLYQKKIQDLSDNREK"
n2_unstructured4 = "D"
n2_helix5 = "E"
n2_unstructured5 = "SE"
n2_helix6 = "LK"
n2_unstructured6 = "KL"
n2_helix7 = "K"
n2_unstructured7 = "TE"
n2_helix8 = "ENSFKRLMIVKKEKLATAQFKINKKHEDVKQYKRTVIEDCNKVQEKRGAVYERVTTINQEIQKIKLGIQQLKDAAEREKLKSQEIFLNLKTALEKYHDGIEKAAEDSYAKIDEKTAELKRKMF"

composite_n2_all = n2_helix1+n2_unstructured1+n2_helix2+n2_unstructured2+n2_helix3+n2_unstructured3+n2_helix4+n2_unstructured4+n2_helix5+n2_unstructured5+n2_helix6+n2_unstructured6+n2_helix7+n2_unstructured7+n2_helix8
structured_n2 = n2_helix1+n2_helix2+n2_helix3+n2_helix4+n2_helix5+n2_helix6+n2_helix7+n2_helix8

# This is going from left to right, according to figure
nd_helix1 = "DEMNAELQSKLKDLFNVDAFKLESLEAKNRALNEQIARLEQEREKEPNRLESLRKLKASLQGDVQKYQAYMSNLESHSAILDQKLNGLNEEIARVELECETIKQENTRLQNIIDNQKYSVADIERINHERNELQQTINKLTKDLEAEQQKLWNEELKYARGKEAIETQLAEYHKLARKLKL"
nd_unstructured1 = "IPKGAENSKGY"
nd_bstrand1 = "DFEIKFN" # loop beta sheet
nd_unstructured2 = "PEAGANCLVKYRAQ"
nd_helix2 = "VYVPLKELLNETEEEINKALNKKMGLEDTLEQLNAMITESKRSVRTLKEEVQKLDDLYQQKIKEAEEEDEKCASELESLEKHKHLLESTVNQGLSEAMNELDAVQREYQLVVQTTTEERRKVGNNLQRLLEMVATHVGSVEKHLEEQIAKVDREYEECMSED"

composite_nd_all = nd_helix1+nd_unstructured1+nd_bstrand1+nd_unstructured2+nd_helix2
structured_nd = nd_helix1+nd_helix2

max_length = min([len(composite_nd_all), len(composite_n2_all)])
composite_nd_truncated = composite_nd_all[:max_length]
composite_n2_truncated = composite_n2_all[:max_length]

my_model = SimpleDimer2Phi(max_length, radius, pitch, phica_0, phica_1)
my_model.build()

my_model.pack_new_sequences([composite_nd_truncated, composite_n2_truncated])

# alignment of the two coils based on known locations of cross-links

loop_center_index = 195
alignment_point = loop_center_index-60

composite_nd_aligned = composite_nd_truncated[:alignment_point]
composite_n2_aligned = composite_n2_truncated[5:alignment_point+5]

aligned_model = SimpleDimer2Phi(len(composite_nd_aligned), radius, pitch, phica_0, phica_1)
aligned_model.build()
aligned_model.pack_new_sequences([composite_nd_aligned, composite_n2_aligned])

"""
This is the stuff on the other side of the loop, aligned from 462-209.
"""

composite_nd_aligned_2 = composite_nd_all[237:] # point 462 on diagram, from my knowledge that 420 (diagram) and 196 (my sequence) are aligned
composite_n2_aligned_2 = composite_n2_all[(237-50):] # 50 fewer residues in nuf2 to get to this point. so these sequencse are aligned at this point.

min_length = min([len(composite_nd_aligned_2), len(composite_n2_aligned_2)])

composite_nd_aligned_2 = composite_nd_aligned_2[:min_length]
composite_n2_aligned_2 = composite_n2_aligned_2[:min_length]

aligned_model_rhs = SimpleDimer2Phi(min_length, radius, pitch, phica_0, phica_1)
aligned_model_rhs.build()

aligned_model_rhs.pack_new_sequences([composite_nd_aligned_2, composite_n2_aligned_2])

"""
These are the same as the previous two but they go right up to the edge of the unstructured loop. First is the lhs, then the rhs
"""

#lhs
loop_center_index = 195
alignment_point = loop_center_index-60

composite_nd_aligned_2 = composite_nd_truncated[:(alignment_point+14)] # 14 to the right of the alignment point before the loop starts
composite_n2_aligned_2 = composite_n2_truncated[5:(alignment_point+5+14)] # 14 to the right of the alignment point before the loop starts

aligned_model_lhs_2 = SimpleDimer2Phi(len(composite_nd_aligned_2), radius, pitch, phica_0, phica_1)
aligned_model_lhs_2.build()
aligned_model_lhs_2.pack_new_sequences([composite_nd_aligned_2, composite_n2_aligned_2])

#rhs
composite_nd_aligned_rhs_2 = composite_nd_all[212:] # point 462 on diagram, from my knowledge that 420 (diagram) and 196 (my sequence) are aligned
composite_n2_aligned_rhs_2 = composite_n2_all[(212-50):] # 50 fewer residues in nuf2 to get to this point. so these sequencse are aligned at this point.

min_length_rhs_2 = min([len(composite_nd_aligned_rhs_2), len(composite_n2_aligned_rhs_2)])

composite_nd_aligned_rhs_2 = composite_nd_aligned_rhs_2[:min_length_rhs_2]
composite_n2_aligned_rhs_2 = composite_n2_aligned_rhs_2[:min_length_rhs_2]

aligned_model_rhs_2 = SimpleDimer2Phi(min_length_rhs_2, radius, pitch, phica_0, phica_1)
aligned_model_rhs_2.build()
aligned_model_rhs_2.pack_new_sequences([composite_nd_aligned_rhs_2, composite_n2_aligned_rhs_2])