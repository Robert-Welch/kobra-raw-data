#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 12:57:58 2019

@author: rob
"""

import FFEA_rod
import numpy as np
import matplotlib.pyplot as plt
import global_plot_params
import sys
import copy
import matplotlib.ticker as ticker

import ndc_extractor



def plot_twist_recovery_comparison(trajpath="recover_twist/recover_twist_inhomogeneous/twist.rodtraj", out_filename="twist_recovery_overall.pdf"):
    global_plot_params.setrc(plt)
    rod = FFEA_rod.FFEA_rod(trajpath)
    analysis = FFEA_rod.anal_rod(rod)
    beta, beta_err = ndc_extractor.get_inhomogeneous_param(analysis, 1)
    actual_values = analysis.rod.material_params[0,:,1]
    actual_values = actual_values[1:-1]
    
    with plt.style.context("fast"):
        plt.errorbar(range(len(beta)), beta, yerr=beta_err, label="Recovered values")
        plt.errorbar(range(len(actual_values)), actual_values, label="Original values")
        #plt.semilogy(eigenvalues[:,0], label="$\lambda_{max}$")
        #plt.semilogy(eigenvalues[:,1], label="$\lambda_{min}$", linestyle="--")
        plt.xlabel("Node index")
        plt.ylabel(r"$\beta$ ($nm^2$)")
        legend = plt.legend(loc='lower right', frameon=False)
        plt.tight_layout()
        plt.savefig(out_filename, dpi=300)
        plt.clf()
        plt.close()
        
def get_twist_recovery_progressive(trajpath="recover_twist/recover_twist_inhomogeneous/twist.rodtraj", thin_factor=False):
    global_plot_params.setrc(plt)
    rod = FFEA_rod.FFEA_rod(trajpath)
    analysis = FFEA_rod.anal_rod(rod)
    
    if thin_factor:
        analysis.thin(analysis.rod.num_frames/thin_factor)
    
    try:
        analysis.p_i
    except AttributeError:
        analysis.p_i = analysis.get_p_i(analysis.rod.current_r)
        analysis.equil_p_i = analysis.get_p_i(analysis.rod.equil_r)

    p_i_backup = copy.copy(analysis.p_i)
    #p_i_equil_backup = copy.copy(analysis.equil_p_i)
    
    #actual_values = analysis.rod.material_params[0,:,1]
    #actual_values = actual_values[1:-1]
    
    avg_deltas = []
    avg_error_percent = []
    
    
    for traj_amount in np.linspace(0.01,1,30):
        analysis.p_i = p_i_backup
        
        traj_length = int(traj_amount * len(analysis.p_i))
        analysis.p_i = analysis.p_i[:traj_length]
        
        beta, beta_err = ndc_extractor.get_inhomogeneous_param(analysis, 1)
        delta = np.abs(beta - analysis.rod.material_params[0,:,1][1:-1])
        avg_delta = np.average(delta)
        avg_deltas.append(avg_delta)
        
        error_percent = (delta/analysis.rod.material_params[0,:,1][1:-1])*100
        avg_error_percent.append(np.average(error_percent))
        
        print avg_delta
    
    return avg_deltas, avg_error_percent, np.linspace(0.01,1,30)*100

def plot_twist_recovery_progressive(trajpath="recover_twist/recover_twist_inhomogeneous_long/twist.rodtraj", out_filename="twist_recovery_progressive.pdf"):
    delta, error_percent, length = get_twist_recovery_progressive(trajpath)
    length *= 1e-09 * 1000000
    with plt.style.context("fast"):
        fig, ax = plt.subplots()
        ax.plot(length, error_percent)
        ax.set_yscale("log")
        #plt.errorbar(range(len(actual_values)), actual_values, label="Original values")
        #plt.semilogy(eigenvalues[:,0], label="$\lambda_{max}$")
        #plt.semilogy(eigenvalues[:,1], label="$\lambda_{min}$", linestyle="--")
        ax.set_xlabel("Trajectory length (s)")
        ax.set_ylabel(r"$\beta$ error (%)")

        ax.set_yticks(np.logspace(-0, 2, 10))
        ax.get_yaxis().set_major_formatter(ticker.ScalarFormatter())

        legend = plt.legend(loc='upper right', frameon=False)
        fig.tight_layout()
        fig.savefig(out_filename, dpi=300)
        fig.clf()
        plt.close()
    
def auto():
    plot_twist_recovery_progressive(trajpath="recover_twist/recover_twist_inhomogeneous_long/twist.rodtraj", out_filename="twist_recovery_progressive.pdf")
    plot_twist_recovery_comparison(trajpath="recover_twist/recover_twist_inhomogeneous/twist.rodtraj", out_filename="twist_recovery_overall.pdf")
