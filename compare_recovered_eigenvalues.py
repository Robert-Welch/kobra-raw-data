#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 07:43:12 2019

@author: rob
"""

import FFEA_rod
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys
import global_plot_params

import ndc_extractor

def setrc():
    # todo: better setrc
    global_plot_params.setrc(plt)
    
def plot_eigenvalues(eigenvalues1, eigenvalues2, filename):
    """
    Plot the eigenvalues of the B matrix.
    Params:
        eigenvalues: a 2-d numpy array of eigenvalues, where axis 0 is the
        index on the rod, and axis 1 is the value.
        Filename: file to save to (string).
    Doesn't return anything.
    """
    with plt.style.context("fast"):
        setrc()
        plt.semilogy(eigenvalues1[:,0], label="Original $\lambda_{max}$")
        plt.semilogy(eigenvalues1[:,1], label="Original $\lambda_{min}$")
        plt.semilogy(eigenvalues2[:,0], label="Recovered $\lambda_{max}$", linestyle="--")
        plt.semilogy(eigenvalues2[:,1], label="Recovered $\lambda_{min}$", linestyle="--")
        plt.xlabel("Node index")
        plt.ylabel("Eigenvalue ($m^4 \cdot Pa$)")
        legend = plt.legend(loc='lower right', frameon=False)
        plt.ylim(top=5e-28)
        plt.tight_layout()
        plt.savefig(filename, dpi=300)
        plt.clf()

def plot_error(error, filename, timestep, frames_cut = 17):
    setrc()
    fig1, ax1 = plt.subplots()
    
    ax1.plot(timestep[frames_cut:], error[frames_cut:])
    ax1.set_xlabel("Time (s)")
    ax1.set_ylabel("RMSE of B eigenvalue (fraction)")
    ax1.set_xscale('log')
    ax1.set_yscale('log')
    
    #ax1.set_yticks(np.logspace(-1.1, 1.4, 5))
    ax1.set_yticks(np.logspace(-2, -0.4, 5))
    ax1.get_yaxis().set_major_formatter(ticker.ScalarFormatter())

    fig1.tight_layout()
    fig1.savefig(filename, dpi=300)
    fig1.clf()

def check_convergence(delta_omega):
    di_ratio = []
    for i in range(len(delta_omega)):
        curr_do = delta_omega[:i,:,0]
        ratio = np.average(curr_do)/np.sqrt(np.average(curr_do*curr_do))
        di_ratio.append(ratio)
    return di_ratio

def get_B_error(delta_omega, L_i, max_eigvals_source, min_eigvals_source, dest):
    
    temp = 300
    
    B = []
    for element_no in range(len(delta_omega[0])):
        try:
            B.append(ndc_extractor.get_B_avg(delta_omega, temp, L_i, element_no, covariance=False))
        except np.linalg.LinAlgError:
            B.append(np.matrix([[0,0],[0,0]]))

    B = np.array(B)
    print B #you have to print these or the script crashes, i'm dead fucking serious
    
    B = np.reshape( np.array(B).flatten(), [len(B), 4] )
    for B_value in range(len(B)):
        if B[B_value].all() == np.array([0,0,0,0]).all():
            np.delete(dest.B_matrix, B_value)
    
    dest.B_matrix[0][1:-1] = B
    anal_dest = FFEA_rod.anal_rod(dest)
    eigvals_dest = anal_dest.get_B_eigenvalues()[1:-1]
    
    max_eigenvalues_dest = np.max(eigvals_dest, axis=1)
    min_eigenvalues_dest = np.min(eigvals_dest, axis=1)
    max_min_eigenvalues_dest = np.zeros(np.shape(eigvals_dest))
    max_min_eigenvalues_dest[:,0] = max_eigenvalues_dest
    max_min_eigenvalues_dest[:,1] = min_eigenvalues_dest
    
    err_max = (np.abs(max_eigvals_source - max_eigenvalues_dest))**2
    err_min = (np.abs(min_eigvals_source - min_eigenvalues_dest))**2
    
    err_max_rms_fractional = (np.sqrt(err_max/float(len(delta_omega))))/( (max_eigenvalues_dest+min_eigenvalues_dest)/2)
    err_min_rms_fractional = (np.sqrt(err_min/float(len(delta_omega))))/( (min_eigenvalues_dest+min_eigenvalues_dest)/2)
    
    #err_rms = np.sqrt(np.average([err_min, err_max])/float(len(delta_omega)))
    
    return np.average([err_max_rms_fractional, err_min_rms_fractional])
    
def get_eigvals_errs(source, plot_eigs=False):
    
    anal_source = FFEA_rod.anal_rod(source)
    eigvals_source = anal_source.get_B_eigenvalues()[1:-1]
    
    dest = FFEA_rod.FFEA_rod(num_elements = len(source.current_r[0]))
    
    temp = 300
    
    delta_omega, L_i = ndc_extractor.get_delta_omega(anal_source)
    B = []
    for element_no in range(len(delta_omega[0])):
        B.append(ndc_extractor.get_B_avg(delta_omega, temp, L_i, element_no))

    dest.B_matrix[0][1:-1] = np.reshape(np.array(B), [len(B), 4])
    anal_dest = FFEA_rod.anal_rod(dest)
    eigvals_dest = anal_dest.get_B_eigenvalues()[1:-1]

    
    max_eigenvalues_source = np.max(eigvals_source, axis=1)
    min_eigenvalues_source = np.min(eigvals_source, axis=1)
    max_min_eigenvalues_source = np.zeros(np.shape(eigvals_source))
    max_min_eigenvalues_source[:,0] = max_eigenvalues_source
    max_min_eigenvalues_source[:,1] = min_eigenvalues_source
    
    max_eigenvalues_dest = np.max(eigvals_dest, axis=1)
    min_eigenvalues_dest = np.min(eigvals_dest, axis=1)
    max_min_eigenvalues_dest = np.zeros(np.shape(eigvals_dest))
    max_min_eigenvalues_dest[:,0] = max_eigenvalues_dest
    max_min_eigenvalues_dest[:,1] = min_eigenvalues_dest
    
    #plot_eigenvalues(max_min_eigenvalues_source, max_min_eigenvalues_dest, "eigenvalue_recovery_2.pdf")
    
    errors = []
    sliced = delta_omega[0:-1:3]
    for traj_length in range(len(sliced)-1):
        delta_omega_curr = sliced[:(traj_length+2)]
        errors.append( get_B_error(delta_omega_curr, L_i, max_eigenvalues_source, min_eigenvalues_source, dest) )
        
    return errors, max_eigenvalues_dest, min_eigenvalues_dest, max_eigenvalues_source, min_eigenvalues_source

def auto():
    
    step = 1e-11 #5e-9
    
    source = FFEA_rod.FFEA_rod("ndc80c_sims/structure/source.rodtraj")
    iter1 = FFEA_rod.FFEA_rod("B_recovery/sub2/iter1/iter1/rod_sub2x.rodtraj")
    iter2 = FFEA_rod.FFEA_rod("B_recovery/sub2/iter2/rod_sub2x.rod")
    
    errors, max1_dest, min1_dest, max1_source, min1_source = get_eigvals_errs(source)
    errors2, max2_dest, min2_dest, max2_source, min2_source = get_eigvals_errs(iter1)
    #errors3, max3_dest, min3_dest, max3_source, min3_source = get_eigvals_errs(iter2)
    print("done1")
    #errors2, max2_dest, min2_dest, max1_source, min1_source = get_eigvals_errs(source2)
    #print("done2")
    #errors3, max3_dest, min3_dest, max1_source, min1_source = get_eigvals_errs(source3)
    #print("zzz")
    
    #errors = (np.array(errors1)+np.array(errors2)+np.array(errors3))/3.
    
    #initial
    eigs_max = (max1_dest)
    eigs_min = (min1_dest)
    
    max_min_eigenvalues_dest = np.zeros([len(eigs_max), 2])
    max_min_eigenvalues_dest[:,0] = eigs_max
    max_min_eigenvalues_dest[:,1] = eigs_min
    
    #iter1
    eigs_max_iter1 = (max2_dest)
    eigs_min_iter1 = (min2_dest)
    
    max_min_eigenvalues_dest_iter1 = np.zeros([len(eigs_max_iter1), 2])
    max_min_eigenvalues_dest_iter1[:,0] = eigs_max_iter1
    max_min_eigenvalues_dest_iter1[:,1] = eigs_min_iter1
    
    #iter2
    max_min_eigenvalues_source = np.zeros([len(min1_source), 2])
    max_min_eigenvalues_source[:,0] = max1_source
    max_min_eigenvalues_source[:,1] = min1_source
    
    #actual values
    anal_iter2 = FFEA_rod.anal_rod(iter2)
    eigvals_iter2 = anal_iter2.get_B_eigenvalues()[1:-1]
    max_eigenvalues_iter2 = np.max(eigvals_iter2, axis=1)
    min_eigenvalues_iter2 = np.min(eigvals_iter2, axis=1)
    max_min_eigenvalues_iter2 = np.zeros(np.shape(eigvals_iter2))
    max_min_eigenvalues_iter2[:,0] = max_eigenvalues_iter2
    max_min_eigenvalues_iter2[:,1] = min_eigenvalues_iter2
    
    max_min_eigenvalues_source = np.zeros([len(min1_source), 2])
    max_min_eigenvalues_source[:,0] = max1_source
    max_min_eigenvalues_source[:,1] = min1_source
    
    #plot_eigenvalues(max_min_eigenvalues_source[:-4], max_min_eigenvalues_dest[:-4], "eigenvalue_recovery_initial.pdf")
    #plot_eigenvalues(max_min_eigenvalues_source[:-4], max_min_eigenvalues_dest_iter1[:-4], "eigenvalue_recovery_iter1.pdf")
    #plot_eigenvalues(max_min_eigenvalues_source[:-4], max_min_eigenvalues_iter2[:-4], "eigenvalue_recovery_iter2.pdf")
    plot_error(errors, "B_eig_errors_v8.pdf", step*np.linspace(0, len(errors)*3, num=len(errors)))
