#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 12:57:18 2019

@author: rob
"""

def setrc(plt):
    """
    Set global parameters for plotting.
    """
    plt.rcParams['figure.dpi'] = 600
    plt.rcParams['savefig.dpi'] = 600
    plt.rcParams['font.size'] = 6
    plt.rc('font', size=6)          # controls default text sizes
    plt.rc('axes', titlesize=6)     # fontsize of the axes title
    plt.rc('axes', labelsize=6)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=6)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=6)    # fontsize of the tick labels
    plt.rc('legend', fontsize=6)    # legend fontsize
    plt.rc('figure', titlesize=6)  # fontsize of the figure title
    plt.rcParams['figure.figsize'] = (3.26772, 2.45079000001) # for paper only