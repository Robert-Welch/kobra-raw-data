#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 14:48:36 2019

@author: rob
"""

import numpy as np
import matplotlib.pyplot as plt
import FFEA_pdb
import global_plot_params

def plot_modes(mode1, mode2, filename="first_mode_comparison.pdf"):
    global_plot_params.setrc(plt)
    fig, ax = plt.subplots(nrows=1, ncols=2)
    
    for axis in ax:
        axis.set_yticklabels([])
        axis.set_xticklabels([])
        axis.tick_params(axis=u'both', which=u'both',length=0)
        axis.set_aspect('equal')

    for frame in mode1:
        ax[0].plot(frame[:,0], frame[:,1])
    for frame in mode2:
        ax[1].plot(frame[:,0], frame[:,1])
    
    plt.tight_layout()
    plt.savefig(filename, bbox_inches='tight')
    plt.clf()
    plt.close()

def make_2d_frames(frames, plane_x, plane_y):
    frames_2 = np.zeros( [len(frames), len(frames[0].pos), 2] )
    for frame_index in range(len(frames)):
        for node in range(len(frames[frame_index].pos)):
            frames_2[frame_index][node][0] = np.dot(frames[frame_index].pos[node], plane_x)
            frames_2[frame_index][node][1] = np.dot(frames[frame_index].pos[node], plane_y)
    return frames_2

def rotate(frames_2d, radians):
    rotated = np.empty(np.shape(frames_2d))
    for frame in range(len(frames_2d)):
        c, s = np.cos(radians), np.sin(radians)
        R = np.array([ [c, -s], [s, c] ])
        rotated[frame] = R.dot(frames_2d[frame].T).T
    return rotated

def make_2d_pdb(pdb_path):
    
    def norm(x):
        return x/np.linalg.norm(x)
    
    source_pdb = FFEA_pdb.FFEA_pdb(pdb_path)
    
    avg_config = np.zeros( np.shape(source_pdb.chain[0].frame[0].pos))
    for frame in source_pdb.chain[0].frame:
        avg_config += frame.pos
    avg_config /= len(source_pdb.chain[0].frame)
    
#    avg_seg1 = norm(avg_config[6] - avg_config[0])
#    avg_seg2 = norm(avg_config[-1] - avg_config[8])
    avg_seg1 = norm(avg_config[4] - avg_config[0])
    avg_seg2 = norm(avg_config[-1] - avg_config[9])
    
    avg_normal = norm(np.cross(avg_seg1, avg_seg2))
    avg_x = avg_seg1
    avg_y = norm(np.cross(avg_x, avg_normal))
    
    frames_2d = make_2d_frames(source_pdb.chain[0].frame, avg_x, avg_y)
    
    return frames_2d

def make_2d_pdb_newaxis(pdb_path, before_hinge_index=4, after_hinge_index=8):
    
    def norm(x):
        return x/np.linalg.norm(x)
    
    source_pdb = FFEA_pdb.FFEA_pdb(pdb_path)
    
    
    state1 = source_pdb.chain[0].frame[5].pos
    state2 = source_pdb.chain[0].frame[15].pos
    
    plane_vecs = state2 - state1
    
    avg_seg1 = norm(np.average(plane_vecs[:4], axis=0))
    avg_seg2 = norm(np.average(plane_vecs[9:], axis=0))
    
    
#    avg_config = np.zeros( np.shape(source_pdb.chain[0].frame[0].pos))
#    for frame in source_pdb.chain[0].frame:
#        avg_config += frame.pos
#    avg_config /= len(source_pdb.chain[0].frame)
    
#    avg_seg1 = norm(avg_config[6] - avg_config[0])
#    avg_seg2 = norm(avg_config[-1] - avg_config[8])
#    avg_seg1 = norm(avg_config[4] - avg_config[0])
#    avg_seg2 = norm(avg_config[-1] - avg_config[9])
    
    avg_normal = norm(np.cross(avg_seg1, avg_seg2))
    avg_x = avg_seg1
    avg_y = norm(np.cross(avg_x, avg_normal))
    
    frames_2d = make_2d_frames(source_pdb.chain[0].frame, avg_x, avg_y)
    
    return frames_2d

def make_2d_pdb_from_two(source_path, dest_path):
    
    source_pdb = FFEA_pdb.FFEA_pdb(source_path)
    dest_pdb = FFEA_pdb.FFEA_pdb(dest_path)
    
    def norm(x):
        return x/np.linalg.norm(x)
    
    def grab_seg(pdb):
        
        state1 = pdb.chain[0].frame[5].pos
        state2 = pdb.chain[0].frame[15].pos
        
        plane_vecs = state2 - state1
        
        avg_seg1 = norm(np.average(plane_vecs[:7], axis=0))
        avg_seg2 = norm(np.average(plane_vecs[7:], axis=0))
        
        return avg_seg1, avg_seg2

    avg_seg1_source, avg_seg2_source = grab_seg(source_pdb)
    avg_seg1_dest, avg_seg2_dest = grab_seg(dest_pdb)

    avg_seg1 = np.average([avg_seg1_source, avg_seg1_dest], axis=0)
    avg_seg2 = np.average([avg_seg2_source, avg_seg2_dest], axis=0)

    avg_normal = norm(np.cross(avg_seg1, avg_seg2))
    avg_x = avg_seg1
    avg_y = norm(np.cross(avg_x, avg_normal))
    
    source_frames_2d = make_2d_frames(source_pdb.chain[0].frame, avg_x, avg_y)
    dest_frames_2d = make_2d_frames(dest_pdb.chain[0].frame, avg_x, avg_y)
    
    return source_frames_2d, dest_frames_2d

def make_2d_pdb_from_two_v2(source_path, dest_path):
    
    source_pdb = FFEA_pdb.FFEA_pdb(source_path)
    dest_pdb = FFEA_pdb.FFEA_pdb(dest_path)
    
    def norm(x):
        return x/np.linalg.norm(x)
    
    def grab_seg(pdb):
        
        state1 = pdb.chain[0].frame[5].pos
        state2 = pdb.chain[0].frame[15].pos
        
        plane_vecs = state2 - state1
        
        avg_seg1 = norm(np.average(plane_vecs, axis=0))
        
        endtoend_1 = state1[-1] - state1[0]
        endtoend_2 = state2[-1] - state2[0]
        endtoend_avg = norm( np.average([endtoend_1, endtoend_2], axis=0) )
        
        return avg_seg1, endtoend_avg

    avg_seg1_source, avg_seg2_source = grab_seg(source_pdb)
    avg_seg1_dest, avg_seg2_dest = grab_seg(dest_pdb)

    avg_seg1 = np.average([avg_seg1_source, avg_seg1_dest], axis=0)
    avg_seg2 = np.average([avg_seg2_source, avg_seg2_dest], axis=0)

    avg_normal = norm(np.cross(avg_seg1, avg_seg2))
    avg_x = avg_seg1
    avg_y = norm(np.cross(avg_x, avg_normal))
    
    source_frames_2d = make_2d_frames(source_pdb.chain[0].frame, avg_x, avg_y)
    dest_frames_2d = make_2d_frames(dest_pdb.chain[0].frame, avg_x, avg_y)
    
    return source_frames_2d, dest_frames_2d

def auto():
    source, dest = make_2d_pdb_from_two_v2("ndc80c_sims/pcaanims/source_PCAanim_anim1.pdb", "ndc80c_sims/pcaanims/dest_PCAanim_anim1.pdb")
    source = rotate(source, 1.2)
    dest = rotate(dest, 1.2)
    plot_modes(source, dest)
    
    source, dest = make_2d_pdb_from_two_v2("ndc80c_sims/pcaanims/source_PCAanim_anim2.pdb", "ndc80c_sims/pcaanims/dest_PCAanim_anim2.pdb")
    source = rotate(source, 0.5)
    dest = rotate(dest, 0.5)
    plot_modes(source, dest, filename="modes_2_comparison.pdf")
    
    source, dest = make_2d_pdb_from_two_v2("ndc80c_sims/pcaanims/source_PCAanim_anim3.pdb", "ndc80c_sims/pcaanims/dest_PCAanim_anim3.pdb")
    source = rotate(source, 1.6)
    dest = rotate(dest, 1.6)
    plot_modes(source, dest, filename="modes_3_comparison.pdf")
    
    source, dest = make_2d_pdb_from_two_v2("ndc80c_sims/pcaanims/source_PCAanim_anim4.pdb", "ndc80c_sims/pcaanims/dest_PCAanim_anim4.pdb")
    source = rotate(source, -1.2)
    dest = rotate(dest, -1.2)
    plot_modes(source, dest, filename="modes_4_comparison.pdf")
    
    source, dest = make_2d_pdb_from_two_v2("ndc80c_sims/pcaanims/source_PCAanim_anim5.pdb", "ndc80c_sims/pcaanims/dest_PCAanim_anim5.pdb")
    source = rotate(source, 1.8)
    dest = rotate(dest, 1.8)
    plot_modes(source, dest, filename="modes_5_comparison.pdf")

def auto_old():
    
    source = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/source_PCAanim_anim1.pdb")
    dest = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/dest_PCAanim_anim1.pdb")
    #source = rotate(source, 0.5)
    #dest = rotate(dest, 0.5)
    source = rotate(source, -0.4)
    dest = rotate(dest, 2.8)
    plot_modes(source, dest)
    
    source = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/source_PCAanim_anim2.pdb")
    dest = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/dest_PCAanim_anim2.pdb")
    #source = rotate(source, 0.5)
    #dest = rotate(dest, 0.5)
    source = rotate(source, 2.3)
    dest = rotate(dest, 1.7)
    plot_modes(source, dest, filename="modes_2_comparison.pdf")
    
    source = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/source_PCAanim_anim3.pdb")
    dest = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/dest_PCAanim_anim3.pdb")
    #source = rotate(source, 0.5)
    #dest = rotate(dest, 0.5)
    source = rotate(source, 0.0)
    dest = rotate(dest, -0.1)
    plot_modes(source, dest, filename="modes_3_comparison.pdf")
    
    source = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/source_PCAanim_anim4.pdb")
    dest = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/dest_PCAanim_anim4.pdb")
    #source = rotate(source, 0.5)
    #dest = rotate(dest, 0.5)
    source = rotate(source, -0.4)
    dest = rotate(dest, 0.3)
    plot_modes(source, dest, filename="modes_4_comparison.pdf")
    
    source = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/source_PCAanim_anim5.pdb")
    dest = make_2d_pdb_newaxis("ndc80c_sims/pcaanims/dest_PCAanim_anim5.pdb")
    source = rotate(source, -0.3)
    dest = rotate(dest, 1.5)
    plot_modes(source, dest, filename="modes_5_comparison.pdf")
