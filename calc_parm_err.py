#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 10:31:07 2020

@author: rob
"""

import FFEA_rod
import uncertainties
import numpy as np

def halve_traj(source_path, out_path, second_half=False):
    source = FFEA_rod.FFEA_rod(source_path)
    target_num_frames = source.num_frames/2
    source.num_frames = target_num_frames
    if second_half:
        source.current_r = source.current_r[target_num_frames:]
        source.current_m = source.current_m[target_num_frames:]
        source.equil_r = source.equil_r[target_num_frames:]
        source.equil_m = source.equil_m[target_num_frames:]
    source.write_rod(out_path)
    
def get_params_average(analysis, half_kbT):

    analysis.get_stretch_energy()
    half_stretch_squared_avg = np.average(analysis.stretch_energy)
    kappa_stretch = half_kbT/half_stretch_squared_avg
    
    analysis.get_bending_response_mutual()
    half_bend_squared_avg = np.nanmean(analysis.bending_energy)
    B_isotropic = half_kbT/half_bend_squared_avg
    
    analysis.get_twist_amount()
    half_twist_squared_avg = np.average(analysis.twist_energy)
    beta = half_kbT/half_twist_squared_avg
    
    return [kappa_stretch, B_isotropic, beta]

        
def auto():
    
    temp = 300 #kelvin
    analytical_kbT = temp*1.38064852 * 10**-23
    
    half_kbT = 0.5*analytical_kbT
    

    
    source_path = "ndc80c_sims/structure/source.rodtraj"
    halve_traj(source_path, "ndc80c_sims/structure/errors/source_firsthalf.rodtraj", second_half=False)
    halve_traj(source_path, "ndc80c_sims/structure/errors/source_secondhalf.rodtraj", second_half=True)
    second_half = FFEA_rod.FFEA_rod("ndc80c_sims/structure/errors/source_secondhalf.rodtraj")
    first_half = FFEA_rod.FFEA_rod("ndc80c_sims/structure/errors/source_firsthalf.rodtraj")
    first_half_analysis = FFEA_rod.anal_rod(first_half)
    second_half_analysis = FFEA_rod.anal_rod(second_half)
        
    radius = 5e-9
    
    FFEA_rod.rod_creator.set_params(first_half_analysis.rod, 1, 1, radius, 1)
    FFEA_rod.rod_creator.set_params(second_half_analysis.rod, 1, 1, radius, 1)
    
    first_half_parms = get_params_average(first_half_analysis, half_kbT)
    second_half_parms = get_params_average(second_half_analysis, half_kbT)
    
    stretch = np.average(first_half_parms[0], second_half_parms[0])
    twist = np.average(first_half_parms[1], second_half_parms[1])
    bend = np.average(first_half_parms[2], second_half_parms[2])
    
    stretch_err = abs(first_half_parms[0] - second_half_parms[0])
    twist_err = abs(first_half_parms[1] - second_half_parms[1])
    bend_err = abs(first_half_parms[2] - second_half_parms[2])
    
    stretch_unc = uncertainties.ufloat(stretch, stretch_err)
    twist_unc = uncertainties.ufloat(twist, twist_err)
    bend_unc = uncertainties.ufloat(bend, bend_err)
    
    print(stretch_unc)
    print(twist_unc)
    print(bend_unc)
