import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy as np
import global_plot_params

def setrc():
    global_plot_params.setrc(plt)

def main(plot_standard_energy=True):
    
    omp_scaling = np.genfromtxt("scaling_openMP_arc2.csv", skip_header=1, delimiter=",")
    single_thread_scaling = np.genfromtxt("scaling_single_thread_arc2.csv", skip_header=1, delimiter=",")
    
    serial_rod_elements = single_thread_scaling[:,0]
    serial_perf_mutual = single_thread_scaling[:,1]
    serial_perf_nohinge = single_thread_scaling[:,2]
    
    parallel_rod_threads = omp_scaling[:,0]
    parallel_perf_mutual = omp_scaling[:,1]
    parallel_perf_nohinge = omp_scaling[:,2]
    
    with plt.style.context("fast"):
        setrc()
        #fig = plt.figure()
        
        fig, ax = plt.subplots()

        
        plt.plot(serial_rod_elements, serial_perf_mutual, label="Mutual material axis")# rod algorithm single-thread scaling
        if plot_standard_energy:
            plt.plot(serial_rod_elements, serial_perf_nohinge, label="Standard bending energy", linestyle="--")
        plt.xlabel(r'Rod length (segments)')
        plt.ylabel(r'Timestep time ($\mu s$)')
        
        ax.set_aspect(0.04)
        if plot_standard_energy:
            legend = plt.legend(loc='upper left', frameon=False)
        plt.tight_layout()
        plt.savefig("single_thread_scaling.pdf", dpi=300)
        plt.clf()
        
        fig1, ax1 = plt.subplots()
        #ax1.set_yticks([20, 200, 500])
        #ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        #ax1.yaxis.grid(True, which='minor')
        plt.yscale("log")
        plt.xscale("log")
        
        plt.tick_params(axis='y', which='both')
        locmin = plticker.LogLocator(base=10, subs=(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0), numticks=10)
        ax1.yaxis.set_minor_locator(locmin)
        ax1.yaxis.set_minor_formatter(plticker.NullFormatter())

        ax1.plot(parallel_rod_threads, parallel_perf_mutual, label="Mutual material axis")# rod algorithm single-thread scaling
        if plot_standard_energy:
            plt.plot(parallel_rod_threads, parallel_perf_nohinge, label="Standard bending energy", linestyle="--")
        ax1.yaxis.set_minor_formatter(plticker.FormatStrFormatter("%.0f"))
        ax1.yaxis.set_major_formatter(plticker.FormatStrFormatter(""))
        plt.xlabel(r'Number of threads')
        plt.ylabel(r'Timestep time ($\mu s$)')
        if plot_standard_energy:
            legend = plt.legend(loc='upper right', frameon=False)
        plt.tight_layout()
        plt.savefig("parallel_scaling_loglog.pdf", dpi=300)
        plt.clf()
        plt.close()
    return

def auto():
    main()

if __name__ == "__main__":
    main()