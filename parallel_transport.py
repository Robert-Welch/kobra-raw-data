from vpython import *
import povexport

scene = canvas(title='Example Plot',
     width=900, height=900,
     center=vector(0,0,0), background=color.white, ambient=color.gray(0.4))
     
scene.select()

vec1 = arrow(pos=vector(0,0,0), axis=vector(0.99227788, 0., -0.12403473), shaftwidth=0.04, color=color.blue)
vec2 = arrow(pos=vec1.axis, axis=vector(-0.21693046, -0.,  0.97618706), shaftwidth=0.04, color=color.blue)

mi = arrow(pos=vec1.axis/2, axis=vector(-0.13608276,  0.27216553, -0.95257934), shaftwidth=0.04, color=color.red)
mip1 = arrow(pos=(vec2.axis/2)+vec2.pos, axis=vector(0,1,0.0), shaftwidth=0.04, color=color.red)
miprime = arrow(pos=vec1.axis+(vec2.axis/2), axis=vector(0.94285348, 0.27216553, 0.19223226), shaftwidth=0.04, color=color.green)

rim1 = sphere(pos=vector(0,0,0), radius=0.05, color=color.black)
ri = sphere(pos=vec1.axis, radius=0.05, color=color.black)
rip1 = sphere(pos=vec1.axis+vec2.axis, radius=0.05, color=color.black)

scene.fov = pi/2.5
scene.camera.pos += vector(0,60,0)
scene.center = vec1.axis

scene.forward += vector(0.8,-0.4,0)

inclist = ['colors.inc', 'stones.inc', 'woods.inc', 'metals.inc', 'shapes.inc']
scene.pause('Adjust the camera, then click to export to POV-ray.')
povexport.export(canvas=scene, filename='VPobjects.pov', include_list = inclist)
