#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 14:51:25 2019

@author: rob
"""

import FFEA_pdb
import FFEA_rod

rod = FFEA_rod.FFEA_rod("ndc80c_sims/structure/source.rodtraj")
pdb = FFEA_pdb.FFEA_pdb()
pdb.build_from_rod(rod)
pdb.write_to_file("ndc80c_sims/structure/source.pdb")

rod = FFEA_rod.FFEA_rod("ndc80c_sims/structure/source.rodtraj")
pdb = FFEA_pdb.FFEA_pdb()
pdb.build_from_rod(rod, load_equil=True)
pdb.write_to_file("ndc80c_sims/structure/source_equil.pdb")

rod = FFEA_rod.FFEA_rod("ndc80c_sims/iter1/dest.rodtraj")
pdb = FFEA_pdb.FFEA_pdb()
pdb.build_from_rod(rod)
pdb.write_to_file("ndc80c_sims/iter1/dest.pdb")

rod = FFEA_rod.FFEA_rod("ndc80c_sims/iter1/dest.rodtraj")
pdb = FFEA_pdb.FFEA_pdb()
pdb.build_from_rod(rod, load_equil=True)
pdb.write_to_file("ndc80c_sims/iter1/dest_equil.pdb")

import hinge_analysis_lib
import plot_B_eigenvalues
import plot_B_recovery_iterative
import twist_recovery_progressive_plot
import plot_rmsds
import compare_first_modes
import mode_plotter
import compare_recovered_eigenvalues
import rmsd_convergence
import performance_plotter

hinge_analysis_lib.auto()
plot_B_eigenvalues.auto()
plot_B_recovery_iterative.auto()
twist_recovery_progressive_plot.auto()
plot_rmsds.auto()
compare_first_modes.auto()
mode_plotter.auto()
compare_recovered_eigenvalues.auto()
rmsd_convergence.auto()
performance_plotter.auto()
