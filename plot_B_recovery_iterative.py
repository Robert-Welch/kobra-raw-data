#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 20:00:47 2019

@author: rob
"""

import FFEA_rod
import numpy as np
import matplotlib.pyplot as plt
import global_plot_params
    
def compare_eigs_plot(reference_rod, rod_dict, filename, maxmin=True):
    global_plot_params.setrc(plt)
    with plt.style.context("fast"):
        reference_analysis = FFEA_rod.anal_rod(reference_rod)
        reference_eigenvalues = reference_analysis.get_B_eigenvalues()
        reference_eigenvalues = reference_eigenvalues[1:-1]
        for name, rod in rod_dict.iteritems():
            analysis = FFEA_rod.anal_rod(rod)
            eigenvalues=analysis.get_B_eigenvalues()
            eigenvalues = eigenvalues[1:-1]
            
            if maxmin:
                reference_max = np.max(reference_eigenvalues, axis=1)
                reference_min = np.min(reference_eigenvalues, axis=1)
                eigenvalues_max = np.max(eigenvalues, axis=1)
                eigenvalues_min = np.min(eigenvalues, axis=1)
                reference_eigenvalues[:,0] = reference_min
                reference_eigenvalues[:,1] = reference_max
                eigenvalues[:,0] = eigenvalues_min
                eigenvalues[:,1] = eigenvalues_max
            
            plt.semilogy(reference_eigenvalues[:,0], label="Original $\lambda_{max}$")
            plt.semilogy(reference_eigenvalues[:,1], label="Original $\lambda_{min}$")
            plt.semilogy(eigenvalues[:,0], label="Recovered $\lambda_{max}$", linestyle="--")
            plt.semilogy(eigenvalues[:,1], label="Recovered $\lambda_{min}$", linestyle="--")
            plt.xlabel("Node index")
            plt.ylabel("Eigenvalue ($m^4 \cdot Pa$)")
            legend = plt.legend(loc='lower right', frameon=False)
            plt.ylim(top=5e-27)
            plt.tight_layout()
            plt.savefig(filename, dpi=300)
            plt.close()
    
def auto(do_real_data=False):
    iter0 = FFEA_rod.FFEA_rod("B_recovery/sub2/iter0/rod_sub2x.rodtraj")
    iter1 = FFEA_rod.FFEA_rod("B_recovery/sub2/iter1/rod_sub2x.rodtraj")
    iter2 = FFEA_rod.FFEA_rod("B_recovery/sub2/iter2/rod_sub2x.rodtraj")
    iter3 = FFEA_rod.FFEA_rod("B_recovery/sub2/iter3/rod_sub2x.rod")
    
    compare_eigs_plot(iter0, {"iter1":iter1}, "B_recovery/sub2/eig_recovery_iter1_sub2.pdf")
    compare_eigs_plot(iter0, {"iter2":iter2}, "B_recovery/sub2/eig_recovery_iter2_sub2.pdf")
    compare_eigs_plot(iter0, {"iter3":iter3}, "B_recovery/sub2/eig_recovery_iter3_sub2.pdf")
    
    if do_real_data:
        auto_real()
        
def auto_real():
    source = FFEA_rod.FFEA_rod("ndc80c_sims/iter0/dest.rod")
    dest = FFEA_rod.FFEA_rod("ndc80c_sims/iter1/dest2.rod")
    compare_eigs_plot(source, {"dest":dest}, "eig_recovery_real_data.pdf")
