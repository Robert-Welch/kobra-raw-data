    #!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 15:39:42 2018

@author: rob
"""

import FFEA_rod
import numpy as np
import matplotlib.pyplot as plt
import global_plot_params

def setrc():
    global_plot_params.setrc(plt)
    
def plot_eigenvalues(eigenvalues, filename):
    """
    Plot the eigenvalues of the B matrix.
    Params:
        eigenvalues: a 2-d numpy array of eigenvalues, where axis 0 is the
        index on the rod, and axis 1 is the value.
        Filename: file to save to (string).
    Doesn't return anything.
    """
    with plt.style.context("fast"):
        setrc()
        plt.semilogy(eigenvalues[:,0], label="$\lambda_{max}$")
        plt.semilogy(eigenvalues[:,1], label="$\lambda_{min}$", linestyle="--")
        plt.xlabel("Node index")
        plt.ylabel("Eigenvalue ($m^4 \cdot Pa$)")
        legend = plt.legend(loc='lower right', frameon=False)
        plt.tight_layout()
        plt.savefig(filename, dpi=300)
        plt.clf()
    
def auto():
    """
    This generates the figures seen in the paper 'An elastic Cosserat rod
    algorithm for slender biological objects, validated using the NDC80 protein
    complex (working title).
    """
    dest = FFEA_rod.FFEA_rod("ndc80c_sims/iter1/dest.rodtraj")
    anal_dest = FFEA_rod.anal_rod(dest)
    eigvals = anal_dest.get_B_eigenvalues()[1:-1]
    max_eigenvalues = np.max(eigvals, axis=1)
    min_eigenvalues = np.min(eigvals, axis=1)
    max_min_eigenvalues = np.zeros(np.shape(eigvals))
    max_min_eigenvalues[:,0] = max_eigenvalues
    max_min_eigenvalues[:,1] = min_eigenvalues
    
    plot_eigenvalues(max_min_eigenvalues, "eigenvalues.pdf")
