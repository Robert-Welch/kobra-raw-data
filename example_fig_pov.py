from vpython import *
import povexport

scene = canvas(title='Example Plot',
     width=900, height=900,
     center=vector(0,0,0), background=color.white, ambient=color.gray(0.4))
     
scene.select()

vec1 = arrow(pos=vector(-10,0,0),      axis=vector(10,0,-5), shaftwidth=0.3, color=color.black, headwidth=0.1, headlength=0.1)
vec2 = arrow(pos=vector(0,0,-5),      axis=vector(10,0,5), shaftwidth=0.3, color=color.black, headwidth=0.1, headlength=0.1)

lip1 = arrow(pos=vector(-10,0,0),      axis=vector(10,0,-5)*0.8, shaftwidth=0.4, color=color.blue)
li = arrow(pos=vector(0,0,0-5),      axis=vector(10,0,5)*0.8, shaftwidth=0.4, color=color.blue)

rim1 = sphere(pos=vector(-10,0,0), radius=0.5, color=color.black)
ri = sphere(pos=vector(0,0,-5), radius=0.5, color=color.black)
rip1 = sphere(pos=vector(10,0,0), radius=0.5, color=color.black)
                
#rilabel = label(pos=rim1.pos, text="r<sub>i-1</sub>", yoffset=-22, box=False, line=False, height=30)
#rim1label = label(pos=ri.pos, text="r<sub>i</sub>", yoffset=-22, box=False, line=False, height=30)
#rip1label = label(pos=rip1.pos, text="r<sub>i+1</sub>", yoffset=-22, box=False, line=False, height=30)

mim1 = arrow(pos=lip1.pos+(lip1.axis/2),      axis=vector(0,8,0), shaftwidth=0.4, color=color.red)
n_ax = vec(vec1.axis).cross(vec(mim1.axis))/-11
nim1 = arrow(pos=lip1.pos+(lip1.axis/2),      axis=n_ax, shaftwidth=0.4, color=color.red)

mi = arrow(pos=li.pos+(li.axis/2),      axis=vector(0,8,0), shaftwidth=0.4, color=color.red)
n_ax = vec(vec2.axis).cross(vec(mi.axis))/-11
ni = arrow(pos=li.pos+(li.axis/2),      axis=n_ax, shaftwidth=0.4, color=color.red)

#mim1label= label(pos=mim1.pos+(mim1.axis/2), text="m<sub>i-1</sub>", xoffset=-22, box=False, line=False, height=30, color=color.red)
#nim1label = label(pos=nim1.pos+(nim1.axis/2), text="n<sub>i-1</sub>", yoffset=32, box=False, line=False, height=30, color=color.red)

#milabel= label(pos=mi.pos+(mi.axis/2), text="m<sub>i</sub>", xoffset=22, box=False, line=False, height=30, color=color.red)
#nilabel = label(pos=ni.pos+(ni.axis/2), text="n<sub>i</sub>", yoffset=32, box=False, line=False, height=30, color=color.red)

#lim1label = label(pos=lip1.pos+(lip1.axis/2), text="l<sub>i-1</sub>", yoffset=-22, box=False, line=False, height=30, color=color.blue)
#lilabel = label(pos=li.pos+(li.axis/2), text="l<sub>i</sub>", yoffset=-22, box=False, line=False, height=30, color=color.blue)

#pim1label = label(pos=vec1.pos+(vec1.axis*0.85), text="p<sub>i-1</sub>", yoffset=-22, box=False, line=False, height=30, color=color.black)
#pilabel = label(pos=vec2.pos+(vec2.axis*0.85), text="p<sub>i</sub>", yoffset=-22, box=False, line=False, height=30, color=color.black)

scene.fov = pi/400
scene.camera.pos += vector(0,60,0)
scene.center = vector(0,0,0)

scene.forward += vector(0,-0.4,0)

inclist = ['colors.inc', 'stones.inc', 'woods.inc', 'metals.inc', 'shapes.inc']
scene.pause('Adjust the camera, then click to export to POV-ray.')
povexport.export(canvas=scene, filename='VPobjects.pov', include_list = inclist)
