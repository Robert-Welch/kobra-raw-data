#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 15:54:49 2018

@author: rob
"""

#import MDAnalysis
import FFEA_rod
import numpy as np
import matplotlib.pyplot as plt
import global_plot_params

def setrc():    
    global_plot_params.setrc(plt)

def plot(rmsd_source_time, rmsd_source, rmsd_dest_time, rmsd_dest, filename):
    with plt.style.context("fast"):
        setrc()
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax2 = ax1.twiny()
        line1 = ax1.plot(rmsd_source_time, np.array(rmsd_source)*1e9, 'r', label="All-atom trajectory (AMBER)")
        line2 = ax2.plot(rmsd_dest_time, np.array(rmsd_dest)*1e9, 'b', label="Rod trajectory", linestyle="--")
        #ax2.ticklabel_format(style="sci", scilimits = (-1, 1))
        #ax2.get_yaxis().get_offset_text().set_y(0)
        ax2.xaxis.set_major_locator(plt.MaxNLocator(5))
        
        lns = line1+line2
        labs = [l.get_label() for l in lns]
        ax2.grid(False)
        ax1.legend(lns, labs, loc='upper left', frameon=False)
        
        #legend = plt.legend(loc='lower right', frameon=False)
        
        ax1.set_xlabel("Atomistic time (s)")
        ax2.set_xlabel("Rod time (s)")
        ax1.set_ylabel("RMSD (nm)")

        #plt.legend((line1, line2), ('Atomistic trajectory generated with AMBER', 'Rod trajectory'), frameon=False)
        
        #plt.plot(rmsd_source_time, rmsd_source, label="Atomistic trajectory generated with AMBER")
        #plt.plot(rmsd_dest_time, rmsd_dest, label="Rod trajectory")
        #plt.xlabel("Time (s)")
        plt.ylabel("RMSD (nm)")
        #legend = plt.legend(loc='upper right', frameon=False)
        plt.tight_layout()
        plt.savefig(filename, dpi=300)
        plt.clf()
        plt.close()

def auto():
    
    source_step = 1e-12
    dest_step = 1e-8
    
    source = FFEA_rod.FFEA_rod("ndc80c_sims/structure/source.rodtraj")
    dest = FFEA_rod.FFEA_rod("ndc80c_sims/iter1/dest.rodtraj")
    source_analysis = FFEA_rod.anal_rod(source)
    dest_analysis = FFEA_rod.anal_rod(dest)
    
    dest_analysis.rod.current_r= dest_analysis.rod.current_r[2317:]
    dest_analysis.rod.equil_r= dest_analysis.rod.equil_r[2317:]
    source_analysis.rod.current_r= source_analysis.rod.current_r[7200:]
    source_analysis.rod.equil_r= source_analysis.rod.equil_r[7200:]
    
    converging_rmsd_source = source_analysis.get_time_rmsd(is_aligned=False)
    converging_rmsd_dest = dest_analysis.get_time_rmsd(is_aligned=False)
    
    source_scale = np.array(range(len(converging_rmsd_source)))*source_step
    dest_scale = np.array(range(len(converging_rmsd_dest)))*dest_step
    
    converging_rmsd_source_avg = []
    converging_rmsd_dest_avg = []
    
    for frame in range(len(converging_rmsd_source)):
        converging_rmsd_source_avg.append(np.average(converging_rmsd_source[:frame]))
        
    for frame in range(len(converging_rmsd_dest)):
        converging_rmsd_dest_avg.append(np.average(converging_rmsd_dest[:frame]))
        
    #plot(source_scale, converging_rmsd_source_avg, dest_scale[:14879], converging_rmsd_dest_avg[:14879], "rmsd_convergence.pdf")
    plot(source_scale, converging_rmsd_source, dest_scale[:14879], converging_rmsd_dest[:14879], "rmsd_time.pdf")

    #WEIRD AND WRONG

    #anal_source = FFEA_rod.anal_rod(source)
    #anal_dest = FFEA_rod.anal_rod(dest)
