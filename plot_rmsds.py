# -*- coding: utf-8 -*- 
import FFEA_rod
import matplotlib.pyplot as plt
import global_plot_params

def setrc():
    global_plot_params.setrc(plt)

def plot_rmsds(rmsd_source, rmsd_dest, filename):
    with plt.style.context("fast"):
        setrc()
        plt.plot(rmsd_source*1e9, label="Atomistic trajectory generated with AMBER")
        plt.plot(rmsd_dest*1e9, label="Rod trajectory", linestyle="--")
        plt.xlabel("Node index")
        plt.ylabel("RMSD (nm)")
        legend = plt.legend(loc='upper right', frameon=False)
        plt.tight_layout()
        plt.savefig(filename, dpi=300)
        plt.clf()
    return


def auto():
    source = FFEA_rod.FFEA_rod("ndc80c_sims/structure/source.rodtraj")
    dest = FFEA_rod.FFEA_rod("ndc80c_sims/iter1/dest.rodtraj")

    anal_source = FFEA_rod.anal_rod(source)
    anal_dest = FFEA_rod.anal_rod(dest)
    
    anal_dest.rod.current_r= anal_dest.rod.current_r[2457:14879] # bad-un
    anal_dest.rod.equil_r= anal_dest.rod.equil_r[2457:14879]
    
    anal_source.rod.current_r= anal_source.rod.current_r[7500:]
    anal_source.rod.equil_r= anal_source.rod.equil_r[7500:]
    
    rmsd_source = anal_source.get_node_rmsd(align=True)
    rmsd_dest = anal_dest.get_node_rmsd(align=True)
    plot_rmsds(rmsd_source, rmsd_dest, "ndc80_rmsds.pdf")
